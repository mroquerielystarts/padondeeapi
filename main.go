package main

import (
	_ "padondeeapi/routers"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/lib/pq"
	"os"
	"fmt"
)

func init() {
	orm.RegisterDataBase("default", "postgres", "user=postgres password=postgres host=127.0.0.1 port=5432 dbname=mvp01 sslmode=disable")
}

func main() {
	migConf := os.Args[1:]
	if len(migConf) > 0 && migConf[0] == "run-migration" {
		RunMigrations()
	}

	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}
	beego.Run("localhost")
}


func RunMigrations() {

	  o := orm.NewOrm()

	  o.Using("default")

	  // Database alias.
	  name := "default"

	  // Drop table and re-create.
	  force := true

	  // Print log.
	  verbose := true

	  // Error.
	  err := orm.RunSyncdb(name, force, verbose)
	  if err != nil {
	      fmt.Println(err)
	  }
}
