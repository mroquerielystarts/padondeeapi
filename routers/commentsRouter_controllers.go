package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["padondeeapi/controllers:ActivitiesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:ActivitiesController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:ActivitiesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:ActivitiesController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:ActivitiesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:ActivitiesController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:ActivitiesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:ActivitiesController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:ActivitiesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:ActivitiesController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:ApplicationController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:ApplicationController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:ApplicationController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:ApplicationController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:ApplicationController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:ApplicationController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:ApplicationController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:ApplicationController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:ApplicationController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:ApplicationController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:CategoriesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:CategoriesController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:CategoriesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:CategoriesController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:CategoriesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:CategoriesController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:CategoriesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:CategoriesController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:CategoriesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:CategoriesController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:EventsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:EventsController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:EventsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:EventsController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:EventsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:EventsController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:EventsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:EventsController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:EventsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:EventsController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:EventsUserssController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:EventsUserssController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:EventsUserssController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:EventsUserssController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:EventsUserssController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:EventsUserssController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:EventsUserssController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:EventsUserssController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:EventsUserssController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:EventsUserssController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:LogDetailsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:LogDetailsController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:LogDetailsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:LogDetailsController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:LogDetailsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:LogDetailsController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:LogDetailsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:LogDetailsController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:LogDetailsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:LogDetailsController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:LogTypesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:LogTypesController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:LogTypesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:LogTypesController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:LogTypesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:LogTypesController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:LogTypesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:LogTypesController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:LogTypesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:LogTypesController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:LogsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:LogsController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:LogsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:LogsController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:LogsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:LogsController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:LogsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:LogsController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:LogsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:LogsController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:PaymentMethodsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:PaymentMethodsController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:PaymentMethodsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:PaymentMethodsController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:PaymentMethodsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:PaymentMethodsController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:PaymentMethodsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:PaymentMethodsController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:PaymentMethodsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:PaymentMethodsController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:PaymentsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:PaymentsController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:PaymentsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:PaymentsController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:PaymentsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:PaymentsController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:PaymentsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:PaymentsController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:PaymentsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:PaymentsController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:RoleActivitiessController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:RoleActivitiessController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:RoleActivitiessController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:RoleActivitiessController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:RoleActivitiessController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:RoleActivitiessController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:RoleActivitiessController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:RoleActivitiessController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:RoleActivitiessController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:RoleActivitiessController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:RoleController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:RoleController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:RoleController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:RoleController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:RoleController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:RoleController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:RoleController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:RoleController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:RoleController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:RoleController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:TaxesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:TaxesController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:TaxesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:TaxesController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:TaxesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:TaxesController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:TaxesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:TaxesController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:TaxesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:TaxesController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:TicketTypesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:TicketTypesController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:TicketTypesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:TicketTypesController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:TicketTypesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:TicketTypesController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:TicketTypesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:TicketTypesController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:TicketTypesController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:TicketTypesController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:TicketsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:TicketsController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:TicketsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:TicketsController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:TicketsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:TicketsController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:TicketsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:TicketsController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:TicketsController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:TicketsController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:UsersController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:UsersController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:UsersController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:UsersController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:UsersController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:UsersController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:UsersController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:UsersController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["padondeeapi/controllers:UsersController"] = append(beego.GlobalControllerRouter["padondeeapi/controllers:UsersController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

}
