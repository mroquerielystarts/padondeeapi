// @APIVersion 1.0.0
// @Title beego Test API
// @Description beego has a very cool tools to autogenerate documents for your API
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"padondeeapi/controllers"

	"github.com/astaxie/beego"
)

func init() {
	ns := beego.NewNamespace("/v1",

		beego.NSNamespace("/events",
			beego.NSInclude(
				&controllers.EventsController{},
			),
		),

		beego.NSNamespace("/categories",
			beego.NSInclude(
				&controllers.CategoriesController{},
			),
		),

		beego.NSNamespace("/log_types",
			beego.NSInclude(
				&controllers.LogTypesController{},
			),
		),

		beego.NSNamespace("/application",
			beego.NSInclude(
				&controllers.ApplicationController{},
			),
		),

		beego.NSNamespace("/logs",
			beego.NSInclude(
				&controllers.LogsController{},
			),
		),

		beego.NSNamespace("/log_details",
			beego.NSInclude(
				&controllers.LogDetailsController{},
			),
		),

		beego.NSNamespace("/payments",
			beego.NSInclude(
				&controllers.PaymentsController{},
			),
		),

		beego.NSNamespace("/payment_methods",
			beego.NSInclude(
				&controllers.PaymentMethodsController{},
			),
		),

		beego.NSNamespace("/taxes",
			beego.NSInclude(
				&controllers.TaxesController{},
			),
		),

		beego.NSNamespace("/ticket_types",
			beego.NSInclude(
				&controllers.TicketTypesController{},
			),
		),

		beego.NSNamespace("/tickets",
			beego.NSInclude(
				&controllers.TicketsController{},
			),
		),

		beego.NSNamespace("/activities",
			beego.NSInclude(
				&controllers.ActivitiesController{},
			),
		),

		beego.NSNamespace("/role",
			beego.NSInclude(
				&controllers.RoleController{},
			),
		),

		beego.NSNamespace("/users",
			beego.NSInclude(
				&controllers.UsersController{},
			),
		),

		beego.NSNamespace("/role_activitiess",
			beego.NSInclude(
				&controllers.RoleActivitiessController{},
			),
		),

		beego.NSNamespace("/events_userss",
			beego.NSInclude(
				&controllers.EventsUserssController{},
			),
		),
	)
	beego.AddNamespace(ns)
}
